#!/bin/sh

func_led_on ()
{
    echo 1 > /sys/class/gpio/gpio${1}/value
}

func_led_off ()
{
    echo 0 > /sys/class/gpio/gpio${1}/value
}

BLUE=17
RED=27
GREEN=22

echo "Conncet GPIO ${BLUE}:B."
echo "Connect GPIO ${RED}:R."
echo "Connect GPIO ${GREEN}:G."
echo "Connect GND."

sudo echo $BLUE > /sys/class/gpio/export
sudo echo $RED > /sys/class/gpio/export
sudo echo $GREEN > /sys/class/gpio/export

sudo echo out > /sys/class/gpio/gpio${BLUE}/direction
sudo echo out > /sys/class/gpio/gpio${RED}/direction
sudo echo out > /sys/class/gpio/gpio${GREEN}/direction

echo "done."

echo "Test Start."

func_led_on $BLUE
sleep 1
func_led_off $BLUE
func_led_on $RED
sleep 1
func_led_off $RED
func_led_on $GREEN
sleep 1
func_led_off $GREEN

echo "Test End."
