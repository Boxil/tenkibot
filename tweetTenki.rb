#! /usr/bin/ruby
# coding: utf-8
require 'twitter'
require 'weather-report'
require_relative './.config'

AquesTalkPi="/home/pi/aquestalkpi/AquesTalkPi"
DCMTALK="/home/pi/dcmTalk/dcmTalk.rb"
# check plughw `aplay -l`
PLUGHW=`aplay -l | grep -E ".*カード.*([0-2]).*USB.*" | awk '{print $2}' | sed -e 's/://g'`
APLAY="aplay -D plughw:#{PLUGHW.chomp},0"
CURRENT=File.expand_path(File.dirname($0))


client = Twitter::REST::Client.new do |config|
  config.consumer_key = CONSUMER_KEY
  config.consumer_secret = CONSUMER_SECRET
  config.access_token = ACCESS_TOKEN
  config.access_token_secret = ACCESS_TOKEN_SECRET
end


CITY_STR="千葉"
CITY = WeatherReport.get(CITY_STR)

TELOP_PATH="#{CURRENT}/voice/telop.wav"
MAX_PATH="#{CURRENT}/voice/max.wav"

telop = CITY.today.telop
max = CITY.today.temperature_max

if !telop.nil?
  `#{DCMTALK} "#{CITY.today.telop}" "" "#{TELOP_PATH}"`
end

if !max.nil?
  `#{DCMTALK} #{CITY.today.temperature_max} "" #{MAX_PATH}`
end

`#{APLAY} #{CURRENT}/voice/ohayo.wav`
`#{APLAY} #{CURRENT}/voice/kyonotenki.wav`
`#{APLAY} #{TELOP_PATH}`
`rm #{TELOP_PATH}`

if !max.nil?
  `#{APLAY} #{CURRENT}/voice/saikokion.wav`
  `#{APLAY} #{MAX_PATH}`
  `rm #{MAX_PATH}`
  `#{APLAY} #{CURRENT}/voice/do.wav`
end
`#{APLAY} #{CURRENT}/voice/desu.wav`

if !max.nil?
if max > 30 then
  `#{DCMTALK} とても暑いです。 anger`
elsif max > 27 then
  `#{DCMTALK} ちょっと暑いです。`
elsif max > 20 then
  `#{DCMTALK} 暖かいです。 happiness`
elsif max > 15 then
  `#{DCMTALK} 快適です。 happiness`
elsif max > 10 then
  `#{DCMTALK} 寒いです。`
elsif max > 5 then 
  `#{DCMTALK} とても寒いです。sadness`
else
  `#{DCMTALK} とてもとても寒いです。 sadness`
end
end

if CITY.today.umbrella?
  client.update("@mczh 傘持って行け "+Time.now.to_s)
  `#{APLAY} #{CURRENT}/voice/kasa.wav`
  `echo "1" > #{CURRENT}/umbrella.dat` # for Led Notification
else
  `rm #{CURRENT}/umbrella.dat`
end
